clear all
close all

data=importdata('Known_DMM_TB.txt');

figure()
plot(data.data(:,2),data.data(:,4),'Marker','s','MarkerEdgeColor','r','MarkerFaceColor','w','MarkerSize',8,'LineStyle','none');
hold
plot(data.data(:,3)-data.data(:,5)*46.25e3,data.data(:,5),'Marker','none','LineStyle','-','LineWidth',2,'Color','b');